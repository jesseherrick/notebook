---
title: Chapter 1
date: 2015-08-16
subject: apush
---

New World Beginnings
--------------------

- **Canadian Shield** – The geological core of North America; landmass
    carved by years of glaciers to form N.A.
- Early humans most likely came to America by land bridge.
    - Across the Bering isthmus
    - This land-bridge was eventually flooded, marooning the native
      “immigrants”
    - Approximately 54mil people inhabited N. & S. America [^1]
- These migrants diversified into diff. cultures
    - **Incas** – Peru
    - **Aztecs** – Mexico (sophisticated civilization; culture based
      around maize)
      - **Maize** – Native American corn
    - *The culture of the *Pueblo* peoples based itself on corn*
    - **nation-state** – society where political legitimacy and
      authority overlays a cultural commonality
    - **Cahokia** – Mississippian settlement near present-day St.
      Louis
    - **three-sister-farming** – a system of farming where maize,
      beans, and squash are grown to maximize yield
        - these plants don’t interfere with each other
        - led to higher population density and diversification:
          *Creek*, *Choctaw*, and *Cherokee* people

Indirect Discoverers of the New World: Hello, Europe.
-----------------------------------------------------

- Scandinavians first landed in northeast N.A. circa 1000CE
    - With no govt. backing, nor expansion wanted, the settlement was
        abandoned
- Trade from Europe (esp. N.W. Europe) was far and expensive.
- **caravel** – a type of ship able to sail into the wind
    - allowed for travel along the African coast (and back)
    - Africa had gold, Europeans wanted gold
    - Portuguese navigators set up trading posts
        - slave trade was more accessible
        - slaves worked on sugar plantations in Portugal and later
          Spain
        - **plantation** – large-scale agricultural enterprise growing
          commercial crops (esp. using slave labor)
- These Portuguese navigators traveled further along the African
  coast, searching for a route to Asia
    - **Indies** – Region of southeast Asia; named because all unknown
      Oriental lands were called India
- Spain joined the party
    - due to the marriage of *Ferdinand of Aragon* and *Isabella of
      Castille*
    - since Portugal controlled the southeast ports around the S. tip
      of Africa, Spain went west
- The New World’s “Discovery” was set in motion
    - Europeans wanted cheaper, exotic goods
    - The Portuguese had demonstrated advanced ocean navigation was
      possible
    - Spain now had the infrastructure to get these goods
- **Christopher Columbus** – An Italian (but for the Spanish) voyager
  who failed to discover India, but instead accidentally “discovered”
  the New World.
    - He convinced Spanish monarchs to fund his trip, and set sail to
      “India”
    - On **October 12, 1492**, Columbus’ crew sighted a Bahamian
      island
    - Unable to fathom that he had made a mistake, Columbus mistakenly
      called the natives *Indians*.
        - This name has stuck ever since. (Quite possibly one of the
          biggest misnomers of all time.)
    - **The Columbian Exchange** – A triangle of trade where **Europe
      provided the technology, markets, and capital**; the **New World
      provided the raw materials**; and **Africa provided the
      labor (slaves)**.
        - appx. \nicefrac{3}{5} of crops around the world originated
          in the New World
    - *Columbus returned in 1493* with 1200 men, cattle, pigs,
      and horses.
        - Horses became integrated into Indian culture and tribes
        - Horses allowed for a more militarized society
        - They *also brought **smallpox***, among other diseases,
          which quickly killed off the natives.
        - Within 50 years of Columbus’ arrival, 99.98% of Hispaniola
          natives were dead
        - The Indians in-turn gave *syphilis* back to Europe

The Conquest of Mexico and Peru
-------------------------------

- Europeans found that America had riches
    - esp. the gold in Mexico and Peru
- **Treaty of Tordesillas** – Spain’s claim to Columbus’ territory in
  writing, as well as a portion given to Portugal
- **West Indies** – The Caribbean’s island area; another relic of
  Columbus’ poor geographical skills.
- **the _encomienda_ system** – Spanish government giving a colonist
  the right to *demand tribute* (Christianize) and force labor of the
  Indians in an area (usually in exchange for protection); essentially
  slavery
- **Hernan Cortes** used interpreters to learn about the goings-on of
  the Aztecs.
    - he sought the riches and gold that he heard about
    - so he burned his ships and gathered Indian allies
    - “I’ve got a fever, and the only prescription, is more *gold*.” -
      Hernan Cortes (not sic.)
- _**La Noche Triste**_ (“The Night of Sorrows”) – June 30, 1520:
  Aztecs attacked Cortes and his troops
    - The Spanish allies made a hasty a bloody retreat (losing over
      5000 men)
    - However, Cortes eventually struck back and laid seige to
      Tenochtitlan on **August 13, 1521**.
    - With an assist from Smallpox, Cortes essentially ended the
      Aztec empire.
- Francisco Pizarro defeated the Incas (Peru) in 1532, which **added
  to Spain’s riches**.
    - This increase in \$ supply caused inflation in the
      European economy.
    - This lay the foundations of **capitalism** and our *modern
      banking system*.
- ***mestizo*** – person of mixed Indian and European heritage

Exploration and Imperial Rivalry
--------------------------------

- **Notable Explorers**
    - **_Vasco Nunez Balboa_** – European discoverer of the Pacific
      Ocean
    - **_Juan Ponce de Leon_** – Spanish explorer of Florida (he
      thought it was an island) sought gold (and maybe the *Fountain
      of Youth*?)
    - **_Francisco Coronado_** – Discovered the *Grand Canyon* and the
      *Colorado River* (Spanish)
    - **_Hernando de Soto_** – Sought gold in Florida and discovered
      the *Mississippi River*; abused the Native Americans there,
      then died. (Spanish); claimed S.E. North America for Spain
    - **_John Cabot / Giovanni Caboto_** – English explorer of
      the N.A. northeast; c. 1497-1498
    - **_Giovanni da Verrazano_** – French explorer of the N.A.
      northeast; c. 1524
    - **_Jacques Cartier_** – French explorer of the St. Lawrence
      River; c. 1534
- As more exploration occurred, the Spanish built fortresses and
  borders to secure their trade routes.
- Yet, the Spanish wanted more…
    - Don Juan de Anate led a crew of 83 wagons across the desert.
    - **Battle of Acoma** – c. 1599; The victorious Spanish claimed
      the area: **New Mexico**; Established **Santa Fe** in 1609.
- **Pope’s Rebellion** (1680) – Pueblos destroyed every Catholic
  church and killed hundreds of Spaniards. They took back New Mexico.
- **The Black Legend** – Conquerors only came to torture, kill, steal
  gold, and infect (w/Smallpox).
    - Although they *did* do all of those things; the conquerors did
      it to build an empire.
    - However, the Spaniards did *actually* incorporate the Native
      Americans (provided they convert to Christianity) cpw. the
      English who exiled them.

[^1]: This number may not be accurate due to the limitations of European
    estimation.
