---
title: "Chapter 19: Antebellum Tensions Rise"
date: 2016-01-18
subject: apush
---

## Books on Slavery

- Harriet Beecher Stowe publishes _Uncle Tom's Cabin_ in 1852.
  - Popular anti-slavery novel exploring the horrors of slavery.
  - _Esp._ in the family
- Hinton R. Helper's _The Impending Crisis of the South_
  - Argued that slavery hurt non-slaveholding white Southerners.
  
## Kansas

- In an effort to decide on the issue of slavery through popular
  sovereignty, both pro-slavery and anti-slaveryites rushed into the
  Kansas territory.
- __New England Emigrant Aid Company__ --- sent free laborers to
  Kansas to prevent the establishment of slavery
- The Kansas-Nebraska Aggreement said that Kansas was to be
  slaveholding... but popular sovereignty was supreme

- John Brown enters stage left
  - fierce abolitionist and inane warrior
  - literally dies for the cause

- __Lecompton Constitution__ --- a rigged Kansas constitution which
  favored slavery even on the "without slavery" option
