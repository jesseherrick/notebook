---
title: "Chapter 18: Renewing the Sectional Struggle"
date: 2016-01-10
subject: apush
---

* At the end of Polk's presidency, slavery is still the dividing issue.
* Election of 1848 becomes **Lewis Cass (D)** vs **Zachary Taylor
  (Wh)** vs **Martin Van Buren** (FS)
  * __Cass__ believed in **_popular soverignty_** --- letting the local
    governemnts decide on slavery (banning or protecting it)
  * __Taylor:__ "The Hero of Buena Vista"
    - 12th POTUS
    - Owned slaves
    - Chosen because Clay (father of the Whigs) had made too many enemies.
  * __Van Buren:__ Those that didn't like Cass or Taylor started the
    Free Soil party on the basis of preventing the expansion of
    slavery into Western territory.
      - based this on economic (let the white men have jobs), not
        moral (slavery is wrong) reasons
* Gold fever strikes in 1849 and along with it come diseases into the
  new territory!
  * This was the California Gold Rush!
  * This led to lawlessness and thus Californians desperately rushed
    to statehood... but not so fast, California!
    - The Senate balance was even slave state to non slave state and
      California being admitted as a free state would lead to
      sectional tensions.

> "A house divided against itself cannot stand." -- __Abraham Lincoln__

* Southerners began to fear that the north was conspiring against
  slavery.
* The __underground railroad__ also began a healthy supply of runaway
  slaves.
* __Harriet Tubman__ --- (aka _Moses_) one of the most famous
  conductors of the underground railroad
* Southerns began to demand more strict runaway laws because it was an
  inter-state issue
* __7th of March Speech__ -- Webster's famous speech urging the North
  to support the compromise of 1850 (ruined his political career)
* __William Seward__ --- proclaimed a "higher law" than the
  Constitution, which also made people uneasy
* In 1850, POTUS Taylor died, leaving __Millard Fillmore president__
  - He was in a compromising mood...
* __The Compromise of 1850__
  * California admitted as a _free_ state
  * Utah and New Mexico territory to be decided by popular sovereignty
  * Texas cedes New Mexico and goes to modern day boundaries 
  * No slave trading in Washington DC (but still slavery...)
  * Fugitive Slave Act is strengthened to an alarming and corrupt
    level
* _"Union Savers"_ wanted compromise (like Clay)
* _"Fire Eaters"_ wanted all or nothing (like Callhoun)
* By giving CA to the North, the compromise of 1850 actually favored
  the North by its new Senate majority (on the issue of slavery)
* __Fugitive Slave Law__
  1. Penalized those who helped slaves escape.
  2. Slaves did not have a trial by jury.
  3. Slaves could not testify in their own defense.
  4. Reward money for federal officials higher for convicting someone
     as a slave vs upholding their freedom.
  5. (Polarized the people of the North and South to take a side of
     this appalling law)

