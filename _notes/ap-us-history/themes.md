---
title: 7 Thematic Objectives
date: 2015-08-18
subject: apush
---

- **NAT** – American and National Identity
- **POL** – Politics and Power
- **WXT** – Work, Exchange, and Technology
- **CUL** – Culture and Society
- **MIG** – Migration and Settlement
- **ENV** – Geography and the Environment
- **WOR** – America in the World

