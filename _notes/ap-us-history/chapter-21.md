---
title: "Chapter 21: The Furnace of Civil War"
date: 2016-01-31
---

## Bull Run Ends the "Ninety-Day War"
- **_Bull Run (Manassas Junction)_** -- First major battle of the
  Civil War; Southern victory
  - As often happens at the beginning of wars such as this, the Union
    was taught a swift lesson in overconfidence before going into war.
  - _Thomas J. Jackson_, henceforth referred to as "Stonewall"
    Jackson, led his troops to victory here.
  - Bull Run put the idea of peaceful reunion out of the Union's mind.

## "Tardy George" McClellan and the Peninsula Campaign
- _George B. McClellan_ is appointed commander of the Army of the
  Potomac in 1861.
  - He turns out to be terrible and indecisive, but we'll get to this
    later.
  - He also shows up later in the election of 1864 (#foreshadowing).
  - He was a perfectionist unwilling to take risks to win the war.
- McClellan endlessly drills his men without any advance toward
  Richmond.
  - This got him the nickname "Tardy George"

> "If General McClellan does not want to use the army, I would like to
> borrow it for a time." -- Abraham Lincoln

- **_The Peninsula Campaign_** -- McClellan's waterbound attempt to
  take Richmond; it failed (1862).
- **Robert E. Lee** -- Brilliant Confederate general and tactition;
  often outmaneuvered the Union army with less forces by taking a
  divide-and-conquer approach.
- Ironically, McClellan's loss ensured the eventual end of slavery.
  - because Lincoln still wanted to keep the war about something that
    it wasn't
  - because of the loss, Lincoln was forced to polarize the South
    through his Emancipation Proclamation
- **Union Strategy**
  1. blockade the coasts (no supplies)
  2. emancipate the slaves
  3. take the Mississippi River
  4. send troops throughout the South
  5. take Richmond and win
- **Confederate Strategy**
  1. fight a war of attrition until a new president is elected or a
     foreign country gets involved
     
## The War at Sea
- As the North blockaded the South, blockade-running became profitable
  for those that dared.
- The USS Monitor and CSS Virginia (Merrimack) fought in a well-run
  battle of ironclads to a sort-of stalemate.
  - Warfare was thus changed forever.

## The Pivotal Point: Antietam
- **Second Battle of Bull Run** -- Another Confederate victory by
  Gen. Lee against John Pope.
- **John Pope** -- Union general known for his victories in the west,
  then eventual defeat at 2nd Bull Run in the east.
- **Antietam**
  - Lincoln restores McClellan to his former position as gen. of the
    Army of the Potomac.
  - Union soldiers found Lee's battle plans wrapped around a packet of
    cigars **and still failed at destroying Lee's army**.
  - McClellan was removed once and for all, but this isn't the last
    we'll see of him...
  - It was a very bloody draw.
- **_Emancipation Proclamation_** -- Declared all slaves in rebelling
  states free
  - resulted in flee of thousands af slaves to Union lines
  - yet border state slaves were _still_ not free
  - Lincoln used Antietam as the "victory" he needed to deliver this

## A Proclamation Without Emancipation
- Lincoln's emancipation was more symbollic of an end to compromise
  with the South over the issue of slavery.
  - He couldn't push forth the issue much more in the Union so that
    the slave-holding border states would stay in the Union.
- However, 1 in 7 slaves ran away to Union armies.
  - Further crippling the CSA's infrastructure.
- The proclamation also bolstered the Union's moral basis for war.
- There was also opposition in the north to the proclamation.

## Blacks Battle Bondage
- By the end of the war, over 180,000 blacks served in the Union army.
- The CSS still enlisted 0.

## Leeâ€™s Last Lunge at Gettysburg
- **A. E. Burnside** -- Commander of the Army of the Potomac after
  McClellan's second failure.
  - Attacked Fredericksburg, VA, and failed miserably... it was suicide.
- Burnside yielded command to **Joseph Hooker**.
  - He then loses at Chancellorsville, VA in one of Lee's most
    spectacular victories.
- **George G. Meade** -- Union general after Hooker
  - Won at **Gettysburg**
  - **George Picket** -- CSA Gen. who lost at Gettysburg
  - **Gettysburg** was the beginning of the end of the CSA.

## The War in the West
- **Ulysses S. Grant** -- Became the next Union general.
  - He captured **_Fort Henry and Donelson_** where Grant demanded "an
    unconditional and immediate surrender.
  - **_Shiloh_** -- Battle of the Tennesse-Mississippi border; bloody
    Union victory
- **_Vicksburg_** -- 2 1/2 month siege of Confederate frot on the MI
  River in TN; fell to U.S. Grant and split the South in two

## Sherman Scorches Georgia
- Sherman scorched (literally) across the South

## The Politics of War
- Lincoln's election was in peril at the end of 1864 without a
  decisive victory.

## The Election of 1864
- Lincoln's victory ensured a united United States without slavery
- **_Union Party_** -- Created by radical Republicans and pro-war
  democrats to re-elect Lincoln.
- Andrew Johnson was Lincoln's runningmate to garner more support from
  border states.
  - ...but then Lincoln died and he ruined construction
- Anyway, Lincoln won the election and the war continued for a bit.

## Grant Outlasts Lee
- **_The Wilderness Campaign_** -- a series of bloody clashes between
  Grant and Lee in Virginia leading up to the capture of Richmond.
- Grant was willing to lose men to win a war. Which in this war,
  made the difference.
  - Finally, Lee surrendered to Grant at **_Appomattox Courthouse_**
    where Grant offered generous terms of surrender to an old friend.

## The Martyrdom of Lincoln
- On April 14, 1865, actor John Wilkes Booth shot Abraham Lincoln in
  Ford's Theatre.
- He never had a chance to truly reunite the union

## The Aftermath of the Nightmare
- A major effect of the Civil War is the **British _Reform Bill of
  1867_**, which granted suffrage to all male citizens.
- And finally, America stopped being an "are" and became an "is".
