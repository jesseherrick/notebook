---
title: Hamlet Logs
date: 2015-11-14
subject: apenglish
---

## Resources
- [Text](http://shakespeare.mit.edu/hamlet/)
- [Instructions](/public/hamlet-log-instructions.pdf)

## Act I
### Scene I
- Bernardo, Francisco, Marcellus, and Horatio go ghost-hunting. King
  Hamlet appears but does not speak. The gost plays a very
  significant action in the play. Why has no one told prince Hamlet
  about sighting his father?

### Scene II
- Claudius addresses the public about the impending threat
  (Fortinbras) and his marriage to Gertrude. Laertes wants to go back
  to France... why? Horation tells Hamlet about the ghost. Hamlet
  agrees to watch for it tonight. What is Ophelia's relationship to Hamlet?

### Analysis of Lines 129-159
- Hamlet is angry at his mother for being so weak. (remarrying early,
not mourning enough)
- He calls his father a god compared to whatever his uncle may be.
- Essentially: What are you doing, mother? (Incest)

### Scene III
- _Laertes gives advice to Ophelia_
    - don't think long-term with Hamlet
    - you are too low class for him
    - he might just have sex with you and then leave you behind
- _Polonius gives advice to Laertes_
    - be wise in all aspects. etc.
    - "above all, to thine own self be true"
    - "Give every man thy ear, but few thy voice"
    - advice every parent should give before sending their children
      off to college :wink:
- _Polonius gives advice to Ophelia_
    - he doesn't care about you
    - he just wants to have sex with you
- _Hamlet's getting a pretty bad rap... Why?_

### Scene IV-V
- G-G-G-G-GHOST!
- King Hamlet comes back!
- This time he talks to Hamlet.
- Claudius poisoned King Hamlet in his ear while he was asleep.
    - And worst of all, he's stuck as a ghost now because he died
      before confession!
- This scene is obviously significant.
- Hamlet tells his -- _advisors?_ -- that they are to keep their
mouths shut about what they saw.
- Hamlet is going to act insane... cool, but why does he choose this?

## Act II
### Scene I
- Renaldo, check on my son. Make sure he's not hanging out with
prostitutes, gambling his money away, or addicted to drugs.
    - Make sure he's still courteous even if you beat him in a match of sorts.
- Obviously him forgetting was on purpose... Alzheimers? Is this important?
- "Your bait of falsehood takes this carp of truth"
- _Ophelia enters_
    - __Ophelia:__ Hamlet is insane, father.
    - __Polonius:__ Insane? Nah. He's just crazy in love! Oh my, I
      guess I shouldn't have told you to ignore him... whoops. I
      thought he just wanted a fling. Let's go tell the King about
      this!

- Friends of Hamlet! But spies for the King...
- __Rosencrantz:__ That sounds more like a threat to me.
- The King mixes up their names...
- Norway has negotiated peace! Yay!
- __Polonius:__ Hamlet is just mad!
    - But why?
    - My daughter made him mad!

> "'To the celestial and my soul's idol, the most beautified
Ophelia,'-- That's an ill phrase, a vile phrase; 'beautified' is a
vile phrase: but you shall hear. Thus:"

### Scene II
- To be or not to be speech.

> "Thus conscience does make cowards of us all;"

- Was Ophelia, the King, and Polonius listening the whole time?

> "And, with them, words of so sweet breath composed
As made the things more rich" --- contrasts strongly with her father's
earlier statement about Hamlet's words!

- He tells her that he loves her... and then immediately says otherwise
- Basically: "I am such a terrible person, why should you believe that
I love you?"
- He's playing along with this: He tells her that she is a curse (and
she takes it seriously).
- Claudius isn't so sure...
- more to come!

## Act III
### Scene I

- Rosencrantz and Guildenstern report on Hamlet to the king and a few others.

> "Madam, it so fell out, that certain players
We o'er-raught on the way: of these we told him;
And there did seem in him a kind of joy
To hear of it: they are about the court,
And, as I think, they have already order
This night to play before him." --- Hamlet wants to see the players!
That will bring him joy, right?

- The king plans to send Hamlet to England to cure his madness... or
to get him out of the way. Maybe both?

- Polonius tells him to wait for a bit, to see if Ophelia can make him
  come around from the madness.

> "It shall be so:
Madness in great ones must not unwatch'd go."

### Scene II

> "Be not too tame neither, but let your own discretion be your tutor:
suit the action to the word, the word to the action" --- Hamlet gives
acting advice

- Translation: _There may be idiots in the theatre who laugh at the
  absurdity of your acting, but ignore those people: only a discerning
  few matter._

> "Observe mine uncle: if his occulted guilt Do not itself unkennel in
one speech, It is a damned ghost that we have seen, And my
imaginations are as foul As Vulcan's stithy." --- Hamlet brags to
Horatio about his plan

> "O God, your only jig-maker. What should a man do but be merry? for,
look you, how cheerfully my mother looks, and my father died within
these two hours.

...

O heavens! die two
months ago, and not forgotten yet? Then there's
hope a great man's memory may outlive his life half
a year: but, by'r lady, he must build churches,
then; or else shall he suffer not thinking on, with
the hobby-horse, whose epitaph is 'For, O, for, O,
the hobby-horse is forgot.'
" --- Hamlet to Ophelia; such funny lines

> "In second husband let me be accurst!
None wed the second but who kill'd the first."

> "You're as good as a chorus, my lord."

- Hamlet was right and brags again to Horatio.
- His plan has played out... what next?

### Scene III
- Claudius knows that Hamlet knows, so he tells R. & G. to send him
off to England ASAP.

- Polonius arrives and tells Claudius that he's going to spy on Hamlet
  and his mother... creepy.

- Why can't Gertrude just tell Claudius what she said to Hamlet? Why
  does it have to be Polonius telling the king?

> "And, like a man to double business bound,
I stand in pause where I shall first begin,
And both neglect."

> "To wash it white as snow? Whereto serves mercy
But to confront the visage of offence?
And what's in prayer but this two-fold force,
To be forestalled ere we come to fall,
Or pardon'd being down? Then I'll look up;
My fault is past. But, O, what form of prayer
Can serve my turn? 'Forgive me my foul murder'?" --- Is Claudius
actually repentful of his fratricide?

> "Now might I do it pat, now he is praying;
And now I'll do't. And so he goes to heaven;
And so am I revenged. That would be scann'd:
A villain kills my father; and for that,
I, his sole son, do this same villain send
To heaven.
O, this is hire and salary, not revenge." --- Apparently you shouldn't
kill a man while he's praying because then he'll go to heaven and the
point is moot. Also, how would one act this out on stage? It seems
that Claudius would hear Hamlet.

### Scene IV
- Hamlet finally confronts his mother.

> "No, by the rood, not so:
You are the queen, your husband's brother's wife;
And--would it were not so!--you are my mother."

- Hamlet kills Polonius... by accident-ish?

> "A bloody deed! almost as bad, good mother,
As kill a king, and marry with his brother." --- I love how Hamlet
just immediately turns his murder into a point against his mother.

- Hamlet releases all of his pent up emotion. He tells his mother that
  she married to early and has been unfaithful.

> "Look here, upon this picture, and on this,
The counterfeit presentment of two brothers.
See, what a grace was seated on this brow;
Hyperion's curls; the front of Jove himself;
An eye like Mars, to threaten and command;
A station like the herald Mercury
New-lighted on a heaven-kissing hill;
A combination and a form indeed,
Where every god did seem to set his seal,
To give the world assurance of a man:
This was your husband. Look you now, what follows:
Here is your husband; like a mildew'd ear,
Blasting his wholesome brother. Have you eyes?
Could you on this fair mountain leave to feed,
And batten on this moor? Ha! have you eyes?
You cannot call it love; for at your age" --- Hamlet tells his mother,
_why on earth would you marry this guy after marrying his much
superior brother? (King Hamlet)_

> "**HAMLET**

> Nay, but to live
In the rank sweat of an enseamed bed,
Stew'd in corruption, honeying and making love
Over the nasty sty,--

> **QUEEN GERTRUDE**

> O, speak to me no more;
These words, like daggers, enter in mine ears;
No more, sweet Hamlet!"

- The Ghost comes back, but only Hamlet can see it. Is it even real?
  Is it because of what Hamlet is doing to his mother?

- Hamlet seems scared of the ghost, why?

> "Whilst rank corruption, mining all within,
Infects unseen. Confess yourself to heaven;
Repent what's past; avoid what is to come;
And do not spread the compost on the weeds,
To make them ranker. Forgive me this my virtue;
For in the fatness of these pursy times
Virtue itself of vice must pardon beg,
Yea, curb and woo for leave to do him good." --- Hamlet warns his
mother of what's to come.

- Hamlet is very moody in this scene. He switches between about 4
  different feelings throughout the scene.

## Act IV

_Covered by paper on Laertes._

## Act V
### Scene I
- The two clowns are digging a grave for Ophelia.
- They argue over whether Ophelia killed herself.
- The clown likes to play on words and phrases, often being too literal.
- Hamlet fights with Laertes over who is most saddened by Ophelia's
death.
- Is Hamlet serious, or is he messing with Laertes?

**Some funny lines:**

> "How can that be, unless she drowned herself in her
own defence?"

> "For none, neither." -- some wordplay with Hamlet

> "'Twill, a not be seen in him there; there the men
are as mad as he." --- sounds airhorns; England is just as crazy as Denmark

> "Why, sir, his hide is so tanned with his trade, that
he will keep out water a great while; and your water
is a sore decayer of your whoreson dead body."

**Some serious lines:**

> "That skull had a tongue in it, and could sing once:
how the knave jowls it to the ground, as if it were
Cain's jaw-bone, that did the first murder! It
might be the pate of a politician, which this ass
now o'er-reaches; one that would circumvent God,
might it not?"

> "Now get you to my lady's chamber, and tell her, let
her paint an inch thick, to this favour she must
come; make her laugh at that" --- which lady?

> "Lay her i' the earth:
And from her fair and unpolluted flesh
May violets spring! I tell thee, churlish priest,
A ministering angel shall my sister be,
When thou liest howling." --- "_burn in hell priest, for my sister will
be an angel (regardless of her manner of death)_"

### Scene II
> "For, by the image of my cause, I see
> The portraiture of his: I'll court his favours." --- evidence for
Laertes being Hamlet's foil

> "__HAMLET__

> I will receive it, sir, with all diligence of
spirit. Put your bonnet to his right use; 'tis for the head.

> __OSRIC__

> I thank your lordship, it is very hot.

> __HAMLET__

> No, believe me, 'tis very cold; the wind is
northerly.

> __OSRIC__

> It is indifferent cold, my lord, indeed.

> **HAMLET**

> But yet methinks it is very sultry and hot for my
complexion.

> **OSRIC**

> Exceedingly, my lord; it is very sultry,--as
'twere,--I cannot tell how. But, my lord, his
majesty bade me signify to you that he has laid a
great wager on your head: sir, this is the matter,--

> **HAMLET**

> I beseech you, remember-- (points to hat)" --- So funny!

- Hamlet is making fun of Osric so much. I think it's just fun for
Hamlet to play games with a servant/second.

> "let
the foils be brought, the gentleman willing, and the
king hold his purpose, I will win for him an I can;
if not, I will gain nothing but my shame and the odd hits." --- Hamlet
sees no reason _not_ to accept the wager as it would bring pride to
him or monetary loss for Claudius.

> "If your mind dislike any thing, obey it: I will
forestall their repair hither, and say you are not
fit." --- Horatio tells Hamlet to listen to his heart because
something could be afowl and Horatio _cares for him_.

> "Not a whit, we defy augury: there's a special
providence in the fall of a sparrow. If it be now,
'tis not to come; if it be not to come, it will be
now; if it be not now, yet it will come: the
readiness is all: since no man has aught of what he
leaves, what is't to leave betimes?" --- Is Hamlet accepting death here?

- Hamlet apologizes too; he says he was mad.

> "I'll be your foil, Laertes: in mine ignorance" --- Pretty funny
> because they are foils of eachother...

- Hamlet wins the first two rounds and refuses a drink from Claudius.
- He wins another!

> "And yet 'tis almost 'gainst my conscience." --- Laertes doesn't
> really want to strike Hamlet with the poisoned tip. He's having
> second thoughts, but the king makes him go through with it.

> "She swounds to see them bleed." --- The king trying to cover up the poisoning.

- And then all of them die... except Horatio -- making it a tragedy!
