---
title: Naturalism
date: 2015-09-24
subject: apen
---

## Romanticism vs Naturalism
* __Romanticism__
  * supernatural
  * mystery of nature
  * often symbolic
* __Naturalism__
  * realism
  * _believable_ reality
  * influenced by Darwin's theories of evolution
  * often pessimism

## About Naturalism

