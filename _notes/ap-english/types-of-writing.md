---
title: "Types of Writing (for the AP Test)"
date: 2015-09-17
subject: apen
---

* __3 Types of Writing__
  1. Prose
  2. Poetry
  3. Drama

## Writing in AP English
1. close-read
  - analysis of a prose or poetic passage
  - STIDD, etc.
2. open-ended
  - respond to a prompt
  - based on a previously read novel
3. formal (2 week) essay
  - persuasive/argumentative
  - accomplished & polished piece of work

## Theme
- a 1-2 sentence statement
- __not__ a cliche
- generalizable (doesn't refer to specific characters in the plot)
- __not__ the final word; only an interpretation of the work
