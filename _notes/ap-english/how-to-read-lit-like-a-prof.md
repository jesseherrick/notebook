---
title: Notes on How to Read Literature Like a Professor
date: 2015-08-23
subject: apen
---

How'd He Do That?
-----------------

Interpreting English literature is like interpreting English language:
**There is a previously agreed-upon set of rules to it that everyone
must follow.**

- Being a more experienced reader is like knowing more vocab.

> “Literary language is no different” - **p.XXV**

- **e.g.** point-of-view or tense limitations

- We read fiction on an *emotional level* first.
    - Professors do this too, but they also think deeper.
        1.  Make connections to similar plots.
        2.  Analyze character motivations
    - **Professors are burdened by/with their memory of
      other novels.**

> “Everything is a symbol, it seems, until proven otherwise.” - **XXVII**

- Look for patterns in your reading.
    - Recognize which features are more important than others.
    - Archetypes are sometimes more important than names.

Does He Mean That?
------------------

- **Does the writer actually intend to allude and make things into
    symbols?**
    - *Yes…* Yet, *yes* isn’t exactly true
    - Whereas writers like T.S. Eliot and James Joyce wrote with the
      intention of alluding (i.e. *Ulysses*), we cannot say for sure
      about others.
        - **We cannot prove that an author *intended* to allude
          or symbolize.**
        - Since allusions point to well-recognized works, the author
          may even be *subconsciously* alluding or symbolizing.
        - We should consider that writing lit. is almost always well
          thought out

> “… any aspiring writer is probably also a hungry, aggressive reader
> and will have absorbed a tremendous amount of literary history and
> literary culture … she has access to that tradition in ways that need
> not be conscious” - **p.92**

Don’t Read With Your Eyes
-------------------------

- Don’t read with *your* eyes, read with a set of eyes that are unique
    to the story.
    - Scenes in a story are not what they seem.
    - You are not of the era/location/world that the people of the
      story are.
        - Use eyes that are from that world.
    - **As much fun as it is to take your own knowledge and experience
      into a book, you should remember that the theme is more
      important than the logic.** (see *Sonny’s Blues* ex. on
      **p.234**)

> “… try to find a reading perspective that allows for sympathy with the
> historical moment of the story.” - **p.234**

- **Deconstruction** -- Analyzing a book through extreme questioning to break down its values.

- As much as we should try to sympathize with what the author is
    portraying, we must also keep this sympathy independent from our
    own values.
    - **e.g.** One can read *The Merchant of Venice* and understand
        the Jew’s villainy without being anti-semitic.

> “… you generally want to adopt the worldview the work requests of the
> audience. Sometimes, though … the work asks too much.” - **p.239**


It's Greek to Me
----------------

* __myth__ -- a body of story that _matters_
* since myths are often rooted in similar stories (__e.g.__ all religious stories are pretty much the same), it's easy to confuse mythical allusions with a different story
* myths contain universal human attributes; since humans wrote them
    * because of this, we can draw parallels in books centuries later
    * every single situation that we could possible write about has some sort of Greek or Roman mythical parallel

_An aside:_ This chapter seemed to be more of a corollary to "Does He Mean That?". It makes relevant points about the models set forth in myths about human nature.

Geography Matters
-----------------

> "In a sense, every story or poem is a vacation" - **p.163**

* we often don't think about why a story is set where it is
* geography brings a _culture_ along with it
* **geography is setting**
    * but it also isn't *just* setting
    * geography effects the people that live there
    * it shapes the way they think
    * it shapes the way _we_, as readers, think about the characters

> "geography can be character" - **p.168**

> "when writers send characters south, it's so they can run amok" - __p.171__

...So Does Season
-----------------

* Seasons can be symbols
    * As Shakespeare so often reminds us
    * Spring -- _(re) birth/youth_
    * Summer -- _adulthood/romance_
    * Fall -- _middle age/decline/tiredness/harvest_
    * Winter -- _old age/resentment/death_
* Seasons can be implicit
    * __e.g.__ apple picking
* Seasons are almost always symbols in some way or another.

> "Frost's crop is abundant, suggesting he has done something right, but the effort has worn him out. This, too is part of autumn. As we gather in our harvest, we find we have used up a certain measure of our energies, that in truth we're not as young as we used to be." - __p.180__

It's Never Just Heart Disease...
-----------------------------

> "In literature there is no better, no more lyrical, no more perfectly metaphorical illness than heart disease." - __p.208__

* the heart is so perfectly metaphorical
    * it is the symbolic "repository of emotion"
    * it also keeps us alive
* everything is about the heart, whether symbolic (_heartbreak_) or literal (_death_)
    * [Takotsubo cardiomyopathy](https://en.wikipedia.org/wiki/Takotsubo_cardiomyopathy) anyone?

> "And authors, as a rule, are chiefly intreseted in their characters' humanity." -- __p.210__

* heart _disease_ is often symbolic for "heart" problems
    * so as readers, when we see heart trouble, we should start looking for the more figurative "heart problem"
    * or conversely: when we see "heart problems", we should look for physical heart problems

Marked for Greatness
--------------------

* Originally, physical appearance/marks were referencing to a character's
  morals. By straying from physical perfection, they also stray from moral
  perfection.

> "Sameness doesn't present us with metaphorical possibliies, whereas
> difference---from the average, the typical, the expected---is always rich
> with possibility." -- __p.194__

> "How many stories do you know in which the hero is different from everyone
> else in some way, and how many times is that difference physically
> visible?" -- __p.195__

> "These character markings stand as indicators of the damage life inflicts."
> -- __p.195__

* Life isn't perfect, and it does things to people. These signs manifest
  through physical signs: weariness, stress, scars, etc.
* Oedipus = "Wounded Foot"
* **Physical markings _are_ a character's history.**
* *pssst future essay writing Jesse, this page is important:* p.196 (on
  Oedipus the King)

> "that in each of us, no matter how well made or socially groomed, a
> monstrous Other exists." -- __p.200__

He's Blind for a Reason, You Know
---------------------------------

* Blindness is a major thematic element in _Oedipus Rex_.

> "the author has created a minor constellation of difficulties for himself by
> introducing a blind character into the work, so something important must be
> at stake when blindness pops up in a story" -- __p.202__

* __*e.g.* Tiresias is blind, yet sees the truth.__
* __*e.g.* Oedipus is blind to the truth, then blinds himself.__

> "A truly great story or play ... makes demands on us as readers; in a sense
> it teaches us how to read it." -- __p.204__

* Seeing and blindness are themes introduced in most works. _So why does an
  author specifically add blindness?_ __To make something blatantly obvious.__

> "if you want your audience to know something important about your characters
> (or the work at large), introduce it early, before you need it." -- __p.205__

Is He Serious? And Other Ironies
--------------------------------
* If this was math, __irony is the P in PEMDAS__.
    * Irony > Everything

> "Whereas normally in literary works we watch characters who are our equals
> or even superiors, in an ironic work we watch characters struggle futilely
> with forces we might be able to overcome." -- __p.236__

* Our _expectations_ as to what something is/does makes irony possible.
    * Without preconceived notions of reality, we have no comedy whatsoever,
      actually.
    * So I guess you could say all comedy is based on irony...

> "That's irony---take our expectations and upend them, make them work against
> us." -- __p.238__

* Irony in literature is based on situations and contexts differing from our
  expectations.

> "irony doesn't work for everyone" -- __p.244__

* irony keeps readers compelled to the story and adds depth

If She Comes Up, It's Baptism
-----------------------------
* _My thoughts:_ Maybe authors use almost drowning as a symbolic baptism the
  same way that ancient Roman emperors did: one quick dip to redeem a lifetime
  of wrongdoing. Of course this would have to be a little different in the
  literary version (the characters might _actually_ change rather than just
  die), but you see my point.
* Authors seem to have problems with water... (cough, cough, Virginia Woolf et
  al - p.153)

> "So he's not just alive. He's alive all over again." -- p.154

* So __almost drowning = rebirth__, :heavy_check_mark:, so what?

> "The most famous of these saying is that one cannot step into the same river
> twice. He uses a river to suggest the constantly shifting nature of time: all
> the little bits and pieces that were floating by a moment ago are somewhere
> else now and floating at different rates from each other." -- __p.154__

* We can call most almost/normal drownings some form of "symbolic baptism".
* Wetness breaks stuff, but everything comes out clean on the other side some
way or another.

> "All happy families are the same, but every unhappy one has its own story." --
> __p.161__

It's All About Sex
------------------

* Professors don't have dirty minds, they just recognize the writers'
  dirty minds.
* Freud wasn't necessarily correct about his theories, but they are
  ripe with the basis of sexual innuendo in writing.

> "Suddenly we discover that sex doesn't have to look like sex: other
> objects and activities can stand in for sexual organs and sex acts,
> which is good, since those organs and acts can only be arranged in
> so many ways are not inevitably decorous. -- __p.136__

* Anything can be sexual if the author wills it that way.

> "It isn't wanton or wild sex, but it's still sex." -- __p.137__

> "Every American should know enough of the blues to understand
> exactly what keys and locks signfify ... lances and swords and guns
> as phallic symbols, chalices and grails as symbols of female sexual
> organs." -- __p.139__

> "But symbolically it fulfills the function of masturbation." -- __p.141__

* Why?
    - Writers have been oppressed and censored about sex for centuries.
    - Plausible deniability for the reader. No reading porn at the
      breakfast table.

Except for Sex
--------------

> "The truth is that most of the time when writers deal with sex, they
> avoid writing about the act itself." -- __p.144__

* Writing about sex that has no deeper meaning than the act itself is
just pornography.

> "If they write about sex and mean strictly sex, we have a word for
> that. Pornography."

* The question we should ask is: _Why are they having sex? What will
  the outcome be? What led up to this climax?_

> "... in novels so overheated by passion, the sexiest thing he can do
> is show everything but the lovemaking itself." -- __p.147__

* In summary: _Sex isn't about sex. Sex is about the reasons for the
  act and the consequences of the act._

...More Than It's Gonna Hurt You: Concerning Violence
-----------------------------------------------------

> "Violence is one of the most personal and even intimate acts between
> human beings, but it can also be cultural and societal in its
> implications." -- __p.88__

* I disagree here ("If someone punches you in the nose in a supermarket parking lot, it's simply aggression."): The act itself is the catharsis of emotion created by the tensions that led up to the act. Violence is never just violence, even if it has no meaning.
* (why must you spoil things, Foster? *Ulysses*)
* Deaths with no embodied "killer" do have one major suspect: the author.

> "In the fictive universe, violence is symbolic action." -- __p.91__

> "It's nearly impossibe to generalize about the meaning of violence, except that there are generally more than one, and its range of possibilities is far larger than with something like rain or snow." -- __p.96__

* To paraphrase: _Figure it out yourself. Love, T.C. Foster_
