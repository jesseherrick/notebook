---
title: Sonnets
date: 2016-02-10
---

## Shakespearian
- 14 Lines
  - 3 quatrains (rhyme scheme: __ABAB__, __CDCD__, __EFEF__)
  - 1 heroic couplet (ryhme scheme: __GG__)

## Petrarchan
- 14 Lines
  - 1 octave (8 lines; ryhme scheme: __ABBA__, __ABBA__)
    - creates a situation
  - 1 sestet (6 lines; must have some sort of ryhme scheme)
    - resolves the octave's situation

