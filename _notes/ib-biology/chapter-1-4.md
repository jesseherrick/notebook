---
title: "Chapter 1.4: Cell Transport"
date: 2016-01-19
subject: ibbio
---

## Curriculum
### Understandings
1. Particles move across membranes by simple diffusion, facilitated
diffusion, osmosis and active transport.
2. The fluidity of membranes allows materials to be taken into cells
by endocytosis or released by exocytosis.
3. Vesicles move materials within cells.

### Applications and Skills
1. Application: Structure and function of sodium–potassium pumps for
active transport and potassium channels for facilitated diffusion in
axons.
2. Application: Tissues or organs to be used in medical procedures
must be bathed in a solution with the same osmolarity as the cytoplasm
to prevent osmosis.
3. Skill: Estimation of osmolarity in tissues by bathing samples in
hypotonic and hypertonic solutions. (Practical 2)

## Notes
### Diffusion! :sparkles:
* A form of passive transport.
* What determines a particle going into the cell or not?
  * **Charge & Size!**
  * The heads of the bilayer are polar and the tails are non-polar, so
    the _polar things can't easily pass through, but the non-polar
    things can_
    * small and non-polar go through easily -- ions especially can't
	* __*e.g.*__ $\ce{H_2O}, \ce{CO_2}$, and $\ce{O_2}$

- diffusion works by a __concentration gradient__
	-  things move from high concentrations to low
		- **_i.e._** down the concentration gradient
	- affecting factors (all directly proportional):
		- concentration
		- surface area
		- temperature
		- length of diffusion path
	- adaptations for these factors:
		- build gradient to diffuse faster
		- reduce diffusion path
			- folded membrane increase
			- better surface area to volume ratio (more membrane, less volume)
			- __*e.g.*__ alveoli in lungs
			- __*e.g.*__ root "hairs" for water intake

#### Facilitated Diffusion 
> The use of integral proteins to "pass along" things.

- specific carrier proteins combine with the substance and change shape to transport it _without energy_
- **Ions can go through this.**
- 2 types
	1. **channel protein**
		- gated channel (either *electrically* or *chemically*)
		- or ungated channel
	2. carrier proteins
		- catches a protein and passes it through
		- different substances are attracted to and pass through different proteins *depending on the R-group*
			- these proteins are very specific
- substances _other than water_

#### Osmosis :sweat_drops:
> The diffusion of water through a __selectively permeable membrane__.

- **Water moves *toward* more solute.**
	- *Less* water is located where there is *more* solute, so water moves *down the concentration gradient* toward the area of higher solute.
- Although water can partially diffuse through the plasma membrane on its own, most of it diffuses through protein channels called **_aquaporins_**.
- Types of Solutions
	1. **hypotonic** -- less solute (water moves out)
	2. **hypertonic** -- more solute (water moves in)
	3. **isotonic** -- equilibrium of water (water moves in and out at the same rate; no net movement)
- **turgid** -- when a plant cell swells in a hypotonic solution
- **plasmolysis** -- when plant cells lose water (esp. in a hypertonic solution)
- In medical procedures, isotonic solutions are helpful.
	- IV drips
	- rinsing wounds
	- keeping skin moist before applying grafts
- __*selectively permeable*__ --- only allows certain things through (completely permeable lets everything through)

### Active Transport :zap:
> Moving *against* the concentration gradient using energy (ATP).

- integral protein pumps move substances across the cell membrane **using ATP**
- **_low to high_**

#### Protein Pump :postbox:
- 3 Types of Protein Pumps
	1. **uniport** -- 1 solute, 1 direction
	2. **symport** -- 2 solutes, 1 direction
	3. **antiport** -- 2 solutes, opposite directions

- **Sodium Potassium Pump Steps** (a type of antiporter pump)
  1. 3 *intracellular* sodium ions ($\ce{Na^+}$; within the cell) bind to a specific protein. (The protein opens to allow them in.)
  2. This binding causes ATP phosphorylation: The ATP loses 1 phosphate, resulting in ADP. 1 phosphate group binds to the protein.
  3. This phosphate binding causes the protein to change shape, thus expelling the $\ce{Na^+}$ ions from the cell.
  4. 2 *extracellular* potassium ions ($\ce{K^+}$; outside of the cell) bind to another part inside of the protein and change its shape again, releasing the phosphate group.
  5. The proteins original shape is restored again due to the loss of the phosphate group, releasing the potassium ions back into the cell.

#### Cell Membrane Transport via Endo/Exocytosis :shipit:
- Made possible due to the fluidity of the plasma membrane.
- **vesicle** -- formed by pinching (**_i.e._** budding) a membrane and making a "package"
- Why? *To carry a package safely...*
	- from the rough E.R. to the Golgi
	- from the Golgi to the surface
- They can fuse with membranes of the Golgi or R.E.R., then pinch off into a vesicle again when leaving.
- **Endocytosis**
	- ![Endocytosis Image](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Endocytosis_types.svg/2000px-Endocytosis_types.svg.png)
	- **membrane folds in, or pinches off** (called budding)
		- **phagocytosis** -- cell engulfs large particle by essentially "grabbing" it
		- **pinocytosis** -- (liquid only) intake of fluids by the cell
- **Exocytosis**
	- ![Exocytosis Image](/img/ib-bio/exocyto.gif)
	- The opposite of endocytosis: vesicles fuse with the cell membrane and are pushed out.
	- **Steps of Protein Exocytosis**
		1. Ribosome on the RER makes a protein, which enters the lumen of the ER.
		2. This protein then moves to *cis* side of the Golgi apparatus as a vesicle.
		3. The vesicle fuses to the membrane of the Golgi apparatus and, gets modified, and exits on the *trans* side as a vesicle.
		4. This vesicle moves to the cell membrane, fuses to it, and exits the cell. (exocytosis)
