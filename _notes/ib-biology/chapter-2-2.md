---
title: "Chapter 2.2: Water"
date: 2015-09-19
subject: ibbio
---

## Curriculum
### Understandings
* Water molecules are polar and hydrogen bonds form between them.
* Hydrogen bonding and dipolarity explain the cohesive, adhesive, thermal and
  solvent properties of water.
* Substances can be hydrophilic or hydrophobic.

### Applications and Skills
* Application: Comparison of the thermal properties of water with those of
  methane.
* Application: Use of water as a coolant in sweat.
* Application: Modes of transport of glucose, amino acids, cholesterol, fats,
  oxygen and sodium chloride in blood in relation to their solubility in
  water.

### Guidance
* Students should know at least one example of a benefit to living organisms
  of each property of water.
* Transparency of water and maximum density at 4°C do not need to be included.
* Comparison of the thermal properties of water and methane assists in the
  understanding of the significance of hydrogen bonding in water.

## Adhesive/Cohesive
- In $\ce{H2O}$, the bonds between $\ce{O}$ and $\ce{H}$ are polar.
    - Water is also a __dipolar__ molecule because of its dipole--dipole interactions with other polar molecules.
    - Water diagram (red is O and white is H)![Water Molecule Diagram](https://upload.wikimedia.org/wikipedia/commons/b/b4/Water-3D-balls.png)
- Water molecules are __highly cohesive__
    - highly attracted to each other
    - when two water molecules are near, the positive H ends are attracted to negative O end
        - this is called a __hydrogen bond__
        - although these H-bonds are relatively weak themselves, when multiple water molecules bond, the accumulation of bonds gives water its __surface tension__
    - because of these cohesive forces, water tends to take the smallest possible volume (__i.e.__ water droplets)
- Because it is polar, water is also __adhesive__.
    - attracted to other polar molecules (__e.g.__ glass)

### Special Effects of Properties: Capillary Action
- water's ability to rise up a small tube
- caused by the combination of _cohesive and adhesive forces_
- smaller the tube, higher the climb
- trees use this to get water

## Thermal Properties
- Water has a __high specific heat__
    - its temperature stays relatively stable
    - it can absorb a lot of heat without drastically changing temperature :fire:
    - this is _due to the number of H-bonds_; when one breaks, another immediately forms
    - _Why is this good for life?_
        - Because it gives organisms time to adjust to their environment and maintain homeostasis.
        - Humans are about 70% water, it would be bad if we suddenly were frozen or vaporized from the inside...
- Water has a __high heat of vaporization__
    - Water absorbs a lot of heat when it evaporates
    - _Organisms like us take advantage of this as a cooling mechanism._ (__i.e.__ sweat :sweat_drops:)

## Water as a Solvent
- water is a good solvent __due to its dipolarity__
    - thus, it makes a good __transport medium__
    - metabolic reactions occur readily in water
    - _diffusion_ for certain molecules is also helped by water
- __hydrophillic__ -- "loves water" (polar or ion)
- __hydrophobic__ -- "fears water" (non-polar)

### Transporting Things in Blood
1. glucose
    - polar
    - hydrophillic and thus, _soluble_ in blood plasma
2. amino acids
    - solubility determined by R group
    - R group can be polar, non-polar, or charged
3. cholesterols and fats
    - non-polar, so _not_ soluble in blood plasma
    - __Lipoprotein Complexes__ _are_ soluble and _carry_ them
    - hydrophillic phosphate heads point outward (toward the water/plasma)
    - hydrophobic phosphate tails point inward (against the water/plasma)
4. oxygen
    - oxygen, while _non-polar_, is still soluble to some extent because of how small it is
    - but as the temperature goes up, its solubility goes down so we need a carrier
    - __hemoglobin__ (haemoglobin on the IB test) in red blood cells carry a majority of the oxygen
5. sodium chloride
    - ionic bond
    - freely soluble in water
    - dissolves into $\ce{Na+}$ and $\ce{Cl-}$

## Other Properties of Water
- __expands upon freezing__
    - due to crystal lattice structure, which keeps the atoms at a distance
- __*lower* density when frozen__ 
- __transparent__ (even when frozen)
