---
title: "The Scientific Method or: How I Learned to Stop Worrying and Love IAs"
date: 2015-08-17
subject: ibbio
---

## Overview

**Problem** `->` **Question** `->` **Answer**

But it’s a little more complicated than that.

## Hypothesis

-   if…then question format
-   **e.g.** If the temperature in a specific area is warmer, then
    bacterial growth will increase.

## Variables!

- **independent** -- variable of the experiment being modified
    - **only 1 per experiment**
    - x-axis of a graph
    

- **dependent** -- variable of the experiment changed due to the independent variable
    - **use uncertainties**
    - y-axis of a graph

- **controlled** -- variables kept constant in an experiment
    -   **must list these**
    -   table format is best (**i.e.** *variable: value*)
    -   also must explain *how* they are controlled
    -   **Common Controlled Variables**
        -   light
        -   moisture
        -   humidity
        -   pH
        -   **time** [^1]

## Control Group

-   Group where *no independent variables are modified.*
-   Not to be confused with a control *variable*.

## Materials

-   **Be specific!**
    -   Units, etc.
    -   So someone else could replicate the experiment.

## Procedures

-   Again, imagine you’re explaining it to someone else.
-   Written in **past tense**.
-   List, but use complete sentences.
-   Photo or illustration of lab setup is awesome.
-   Label **variables** as well as equipment.

## Calculating Raw Data

-   Usually just a table is necessary
-   All columns have:
    -   units
    -   headings
-   Try to keep it to one page if possible
-   **_Uncertainties are mandatory!!!_**
-   Include all formulas and calculations used
-   **Include qualitative data as well!**
    -   Descriptions, color, etc.
    -   Add a notes/observations column

## Data Presentations

-   **Always** have a graph
-   At least 1/2 page and 1-2 sentences describing it.
    -   *This is not a conclusion or analysis.*

## Conclusion/Evaluation

-   3-5 paragraphs; if not more
-   paragraph 1 should contain the results
-   **Always base statements off of data.**
    -   Say whether your hypothesis should be *supported* or *refuted*.
    -   Discussed this, don’t state it.

## Limitations

-   1 paragraph on limitations
-   what worked & what didn’t
-   what could have been better designed
-   **outliers** and reasons for them (why?)

## Suggestions for Improvement

-   Based on limitations
-   _**Never** say nothing_
-   Only *useful* improvements
    -   **e.g.** *My equipment was limited in this way: …*

[^1]: Commonly forgotten
