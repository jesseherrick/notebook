---
title: "Chapter 2.1: Organic Molecules to Metabolism"
date: 2015-08-29
subject: ibbio
---

## Curriculum
### Understandings
* Molecular biology explains living processes in terms of the chemical
  substances involved.
* Carbon atoms can form four covalent bonds allowing a diversity of stable
  compounds to exist.
* Life is based on carbon compounds including carbohydrates, lipids, proteins
  and nucleic acids.
* Metabolism is the web of all the enzyme-catalysed reactions in a cell or
  organism.
* Anabolism is the synthesis of complex molecules from simpler molecules
including the formation of macromolecules from monomers by condensation
reactions.
* Catabolism is the breakdown of complex molecules into simpler molecules
including the hydrolysis of macromolecules into monomers.

### Applications and Skills
* Application: Urea as an example of a compound that is produced by living
  organisms but can also be artificially synthesized.
* Skill: Drawing molecular diagrams of glucose, ribose, a saturated fatty acid
  and a generalized amino acid.
* Skill: Identification of biochemicals such as sugars, lipids or amino acids
  from molecular diagrams.

### Guidance
* Only the ring forms of D-ribose, alpha–D-glucose and beta-D-glucose are
  expected in drawings.
* Sugars include monosaccharides and disaccharides.
* Only one saturated fat is expected and its specific name is not necessary.
* The variable radical of amino acids can be shown as R. The structure of
  individual R-groups does not need to be memorized.
* Students should be able to recognize from molecular diagrams that
  triglycerides, phospholipids and steroids are lipids. Drawings of steroids
  are not expected.
* Proteins or parts of polypeptides should be recognized from molecular
  diagrams showing amino acids linked by peptide bonds.

## Notes
- __There are _two_ types of compounds.__
  1. Organic
  2. Inorganic

## Organic Compounds
### The _IB Definition_
- Contain Carbon
- Are found in living organisms.
- __Except__
    1. hydrocarbons
    2. carbonates
    3. oxides of carbon

### What's so special about carbon?
- 4 valence electrons, so it makes cool chains/rings
    - straight chains
    - branched chains
    - rings
- Bonds
    - single
    - double
    - triple

### Functional Groups
- group of atoms that participate in chemical reactions of living organisms
    - affect reactivity
    - make hydrocarbons hydrophillic

![Functional Groups Diagram](https://i.imgur.com/7tIiGrJ.jpg)

- Hydroxyl (Alcohols) 
    - H & O
    - $\ce{-OH}$
- Amino (Amines)
    - $\ce{+NH2}$ or $\ce{-NH3}$
- Carboxyl (Carbolic Acids)
    - carbon atom double bonded to an oxygen atom and single bonded to a hydroxyl group
- Phosphate
    - transfers energy via ATP/GTP
    - $\ce{HOPO(OR)2}$

### Types of Molecules
> Think: <em><strong>CL</strong>i<strong>PN</strong></em>

- Carbohydrates
- Lipids
- Proteins
- Nucleic Acids

### Carbohydrates
* made of C, H, and O
* sugars and starches
* **Learning tip:** Harry Potter's _sweet_-heart is _CHO_ Chang.
* end in _-ose_ usually

#### Function
1. main source of energy
2. complex carbs (starches) store extra sugar
3. structure (for plants)

#### Types
1. **monosaccharide**
    * ratios of $1:2:1$
    * __e.g.__ glucose, fructose, ribose, galactose
        * $\ce{C6H12O6}$
    * **general form:** $C_n H_{2n} O_n$
2. **disaccharides**
    * sucrose = glucose + fructose
    * maltose = glucose + glucose
    * lactose = galactose + glucose
3. **polysaccharides**
    * multiple sugar molecules bonded together
    * **_e.g._**
        * __plant starch__ -- carb. reserve for plants
        * __glycogen__ -- used as long-term energy storage in liver and muscles
        * __cellulose__ -- gives plants structure; makes up cell wall
        * __chitin__ -- structural polysaccharide as well; found in arthropods and cell walls of fungi (rather than cellulose)


### Proteins
* made of C, H, O, and N
* **Learning tip:** My friend named _CHON_ is a body builder; proteins!
    * yeah, it's not as cool as the HP thing
* the monomers (simplest part) of proteins are called _amino acids_
* 20 different amino acids that _commonly occur_ in nature
    * end in _-ine_ usually

#### Function
* __Hormones__ (__e.g.__ insulin)
* __Enzymes__ (__e.g.__ catalase)
* __Immunoglobulin__ (__e.g.__ antibodies)
* __Gas Transport__ (__e.g.__ haemoglobin)
* __Structural__ (__e.g.__ muscles)

#### Structure
__R__ -- is the part of the structure that changes

```
# basic

     NH2 (amino group)
      |
H --- C --- COOH (carboxyl)
      |

# actual

    H    H    O
    |    |   ||
H - N -- C -- C -- OH
         |
         R
```

* intricately *folded*
    * complex molecules
* most of the non-water parts of your body are made of protein
* amino acid + amino acid $\ce{->}$ protein
    * bond between them is called a **peptide bond**
* _peptide_ means protein
* __4 Levels of Protein Structure__ (maybe an image of these 4?)
    1. __primary__ -- the unique sequence of amino acids in a _chain_
    2. __secondary__ -- proteins coil and/or fold
        * __alpha ($\alpha$) helix__ -- a coil
        * __($\beta$) pleated sheet__ -- folding in pleats (like an accordion fold)
    3. __tertiary structure__ -- more folding
        * folding a secondary coil/fold _again_
        * more complex (stacks on top of primary and secondary)
    4. __quaternary__ -- a collaboration structure of multiple chains
        * multiple tertiary structures folded and chained

### Lipids
* __fats__
* __oils__
* __waxes__
* group of organic molecules that are insoluble in water but soluble in non-polar organic solvents
* lipids include triglycerides (fats; oils), phospholipids, and steroids
* __4 Classes of Lipids__
    1. Triglycerides
        * _fats_ and _oils_
        * __the formula:__ 1 glycerol + 3 fatty acid chains
        * **Saturated _vs_ Unsaturated**
            * __saturated__ -- hydrogen above _and_ below carbons
            * __unsaturated__ -- bent; less hydrogen due to _double bonds_
    2. Phospholipids
        * *hydrophillic* head
        * *hydrophobic* tail
        * we'll talk more about this when we get to cell membranes
    3. Waxes
        * composed of long hydrocarbon chains
        * __strongly *hydrophobic*__
        * _highly saturated_
        * solid at room temperature
    4. Sterols (Steroid Alcohols)
        * __e.g.__ cholesterol, some steroidal hormones
        * have a backbone of four carbon rings
        * different steroids created by attaching different _functional groups_ to rings
        * different structures create a different functions
        * __cholesterol__
            * embedded in the cell membrane
            * helps keep membranes _fluid_ and _flexible_

#### Function
* energy storage -- oils in plants and fish; fats in animals; more efficient than carbs
* plasma membrane (phospho*lipid* bilayer!)
* thermal insulation -- fat insulates against heat loss
* buoyancy -- less dense than water
* protection -- shock absorber
* hormones 
* solvents -- dissolves some vitamins (like D!)
* nervous cell insulators

#### Structure

## Metabolism
> The sum of all chemical reactions within a living organism.

* molecules are constantly colliding to undergo reactions
* a large number of factors decide if a reaction occurs
    * molecule identity
    * where they hit
    * speed
* but cells use *enzymes* increase the likelihood of a reaction
* **enzymes** -- protein molecules that have a specific shape (_active site_) into which a reactant (_substrate_) can fit
    * **substrate** -- molecule upon which an enzyme acts
    * **active site** -- the shape a substrate fits into on an enzyme
    * each enzyme can only bind to _one_ type of substrate

### Metabolism = Catabolism + Anabolism
* **catabolism** -- converting large, complex molecules to smaller, simpler forms
    * decomposing biomolecules
    * _e.g._ hydrolysis -- breaking down water to $\ce{H}$ & $\ce{OH}$
* **anabolism** -- converting smaller, simpler molecules into larger, complex forms
    * synthesizing biomolecules
    * _e.g._ dehydration synthesis a.k.a. condensation reactions
* all of these reactions _require enzymes_

## Disproving Vitalism
- __Vitalism__ (completely false theory) -- _living things and their organic molecules can only be created because of a "vital force" required within_
    - Likely a crude attempt to add religion into biology that ultimately failed.
    - __urea__ -- excretes amino acids from the body (using urine)
    - Disproven when Friedrich Wöhler accidentally synthesized urea using two _inorganic salts_.
