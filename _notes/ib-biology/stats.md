---
title: Stats
date: 2015-08-20
subject: ibbio
---

## Measures of Central Tendency

- mean/median/mode
- *what's common in our data*
- **tip** - pick median if an outlier screws up our mean

### Histogram

- when counting
- for *number* comparison, not comparison of specific things
- if smoothed, make a bell curve

### Bar Chart

- when comparing *named* things

## Standard Deviation

- tells how tightly all the various datapoints clustered around the mean in a set of data
- one $\sigma$ away from the mean in either direction is 68%
- 2 $\sigma$ away is 95%
- 3 $\sigma$ away is 99%

### The Formula

\begin{equation}
\sigma = \sqrt{\frac{\sum(x - \overline{x})^2}{n-1}}
\end{equation}

- *small* *$\sigma$* means the values are closer together (thus, more precise)
- *large* *$\sigma$* means more varied data (thus, not precise)

### Error Bars
- **error bar** -- used to show either the range of data or the standard deviation on a graph

## T-Tests
- **A statistical test used to determine significant difference between two sets of data.**
    - _usually something vs control_
    - **basically:** "are they different enough to be significant"
- **degrees of freedom** -- sum of sample sizes __of the two groups__, minus 2
    - _degrees of freedom_ $ = (n_1 + n_2) - 2$
- **observations without an experiment only show _corrolation_**
- **null hypothesis** ($H_0$) -- "There is no significant difference between these two data sets."
    - Another hypothesis that we must accept or refute based on the t-test.

### Calculating T Values

```R
data = read.csv('data/my-data.csv', header = TRUE)
# or
data = read.table('data/my-data-table.csv')

# find t test
t.test(data$x.val, data$y.val, conf.level=.95, var.equal=T)
```

**Or the formula...**

$$
t = \frac{\overline{x_1} - \overline{x_2}}{\sqrt{\frac{(s_1)^2}{n_1} + \frac{(s_2)^2}{n_2}}}
$$

**Where:**

* $\overline{x}$ -- mean
* $n$ -- number of subject
* $s$ -- variance
    * $\frac{\sum{(x_1 - \overline{x_1})^2}}{n_1}$

### Calculating Critical Values
<pre>
# where degrees of freedom = 22
# and confidence level is 95%

qt(.975, df=22)
</pre>

### What does this mean?
* If $t > cv$ ($cv$ = critical value), accept $H_0$ because there is **no** significant difference between the two sets.
* If $t < cv$, reject $H_0$ because there **is** a significant difference between the two sets.

