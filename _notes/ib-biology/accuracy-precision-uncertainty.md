---
title: Accuracy/Precision & Uncertainty
date: 2015-08-18
subject: ibbio
---

## Accuracy and Precision
- **accuracy** – how close to the *correct* answer
- **precision**
    1. how close the answers are to each other
    2. how *exact* a measurement is

## Uncertainty

- *Every* experiment has a degree of uncertainty
- Determining uncertainty (analog/non-electric):
    1. Find the smallest increment on your measurement device
    2. Divide by 2
- Determining uncertainty (digital/electric)
    1. degree of precision is ± the smallest division on the
        instrument
    2. that's it!

