---
title: "Chapter 2.3: Carbohydrates and Lipids"
date: 2015-09-30
subject: ibbio
---

> **Note:** In an effort to add more structure to my notes, I will be using
> section objectives in the IB curriculum as guides for note-taking.

[Chapter 2 IB Objectives](https://ibpublishing.ibo.org/server2/rest/app/tsm.xql?doc=d_4_biolo_gui_1402_1_e&part=3&chapter=2)

## Curriculum
### Understandings
* Monosaccharide monomers are linked together by condensation reactions to
  form disaccharides and polysaccharide polymers.
* Fatty acids can be saturated, monounsaturated or polyunsaturated.
* Unsaturated fatty acids can be cis or trans isomers.
* Triglycerides are formed by condensation from three fatty acids and one
  glycerol.

### Applications and Skills
* Application: Structure and function of cellulose and starch in plants and
  glycogen in humans.
* Application: Scientific evidence for health risks of trans fats and
  saturated fatty acids.
* Application: Lipids are more suitable for long-term energy storage in humans
  than carbohydrates.
* Application: Evaluation of evidence and the methods used to obtain the
  evidence for health claims made about lipids.
* Skill: Use of molecular visualization software to compare cellulose, starch
  and glycogen.
* Skill: Determination of body mass index by calculation or use of a nomogram.

### Guidance
* The structure of starch should include amylose and amylopectin.
* Named examples of fatty acids are not required.
* Sucrose, lactose and maltose should be included as examples of disaccharides
  produced by combining monosaccharides.

## -saccharides
* __monomer__ --- buidling block molecule
* __monosaccharides__ are the _monomers_ of carbohydrates

### Monosaccharides
* **General form:** $C_{n}H_{2n}O_{n}$
  1. __triose__ --- 3 carbons ($\ce{C3H6O3}$)
  2. __pentose__ --- 5 carbons ($\ce{C5H10O5}$)
  3. __hexose__ --- 6 carbons ($\ce{C6H12O6}$)

### Condensation v. Hydrolysis
* __Hydrolysis__ --- polymer + $\ce{H2O ->}$ monomer + monomer
* __Condensation Reaction__ --- monomer + monomer $\ce{-> H2O}$ +
  polymer

### Major Polysaccharides
* __cellulose__ --- support to plant cell walls
* __starch__ --- products of photosynthesis are stored in plants as _starch_
  (as: _starch granules in chloroplasts **or** root structures_)
* __glycogen__ --- animals store excess glucose in this form

## Fatty Acids
* fatty acids have a _carboxyl group_ ($\ce{-COOH}$) at one end and & a methyl
  group ($\ce{CH3 -}$) at the other end
  * with a chain of hydrocarbons in the middle
* __Saturated Fatty Acids__
  * carbons are carrying __as many hydrogens as possible__
  * solid at room temperature
* __Unsaturated Fatty Acids__ -- have at least one double bond (liquid at room
  temp.)
  * __Monounsaturated Fatty Acids__ --- _one_ (1) double bond in the chain of
    hydrocarbons
  * __Polyunsaturated Fatty Acids__ --- at least _two_ (2) double bonds in the
    chain of hydrocarbons

### Triglycerides
* glycerol + 3 fatty acids
* major form of fat in the body
* stored in _adipose tissue_

### Functions of Lipids
* __structure__ --- _phospholipids_ are a main component of cell membranes
* __hormonal signalling__ --- steroids are involved in hormonal signaling
  (__*e.g.*__ estrogen & testosterone)
* __insulation__
  * fats in animals (heat insulators)
  * myelin sheath (electrical insulators)
* __protection__ --- organ cushions
* __storage of energy__ --- triglycerides can be used as a long term energy
  storage

### Why use lipids?
* carbs release less energy then lipids (lipids have 2x the energy)
* _...so why not use them instead?_
  * because glucose is simpler and easier to use
  * no hydrolysis is necessary to use the energy

### Analogy!
* __glucose__ is like :dollar: (money), your body has to spend it
* __glycogen__ is like putting it in your wallet, it's easy to get to: but you
  have to take out your wallet
* __triglycerides__ are like the bank, there's a process to get to it, but you
  can still get it if necessary

### De Bellum Lipids (The War on Lipids or: More on Unsaturated Acids)
* __-cis__ --- hydrogens on the same side (in the hydrocarbon chains); "on
  this same side"
  * very common in nature
  * bent
  * liquid at room temperature
* __-trans__ --- hydrogens on alternating sides (in the hydrocarbon chains);
  "on opposite sides"
  * very rare; mostly artificial
  * straight (because of alternating hydrogens)
  * solid at room temperature

```
CIS fatty acids     TRANS fatty acids

    H  H                  H 
    |  |                  | 
... H==C ...          ... C==C ...
                             |
                             H
```

* OMG, it's CHD!
  * __CHD__ --- coronary heart (:heart:) disease has been correlated with
    intake of trans-fats
  * causes increase of low-density lipoprotein ("bad cholesterol") and
    decrease in high-density lipoprotein ("good cholesterol") 
  * _atherosclerosis_ --- build up of _plaque_ in arteries
    * when hardened, constricts blood flow

### And finally... BMI

> I don't understand the motive of the IB Programme for putting this section
> here, but whatever...

* __BMI__ --- body mass index
* BMI = $(kg)(m^{-2})$
  * __*i.e.*__ $\frac{kg}{m^2}$
  * where $kg$ = mass in kilograms
  * and $m$ = height in meters
