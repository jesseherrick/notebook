---
title: "Chapter 1.1: Intro to Cells"
date: 2015-11-07
subject: ibbio
---

**Essential Idea:** The evolution of multicellular organisms.

## Curriculum
[Link](https://ibpublishing.ibo.org/server2/rest/app/tsm.xql?doc=d_4_biolo_gui_1402_1_e&part=3&chapter=1)

### Understandings
1. According to the cell theory, living organisms are composed of cells.

2. Organisms consisting of only one cell carry out all functions of life in that
cell.

3. Surface area to volume ratio is important in the limitation of cell size.

4. Multicellular organisms have properties that emerge from the interaction of
their cellular components.

5. Specialized tissues can develop by cell differentiation in multicellular
organisms.

6. Differentiation involves the expression of some genes and not others in a
cellb s genome.

7. The capacity of stem cells to divide and differentiate along different
pathways is necessary in embryonic development and also makes stem cells
suitable for therapeutic uses.

### Applications & Skills
1. Application: Questioning the cell theory using atypical examples, including
striated muscle, giant algae and aseptate fungal hyphae.

2. Application: Investigation of functions of life in Paramecium and one named
photosynthetic unicellular organism.

3. Application: Use of stem cells to treat Stargardtb s disease and one other
named condition.

4. Application: Ethics of the therapeutic use of stem cells from specially
created embryos, from the umbilical cord blood of a new-born baby and from an
adultb s own tissues.

5. Skill: Use of a light microscope to investigate the structure of cells and
tissues, with drawing of cells. Calculation of the magnification of drawings and
the actual size of structures and ultrastructures shown in drawings or
micrographs. (Practical 1)

### Guidance
1. Students are expected to be able to name and briefly explain these functions of
life: nutrition, metabolism, growth, response, excretion, homeostasis and
reproduction.

2. Chlorella or Scenedesmus are suitable photosynthetic unicells, but Euglena
should be avoided as it can feed heterotrophically.

3. Scale bars are useful as a way of indicating actual sizes in drawings and micrographs.

### International-mindedness
Stem cell research has depended on the work of teams of scientists in many
countries who share results thereby speeding up the rate of progress. However,
national governments are influenced by local, cultural and religious traditions
that impact on the work of scientists and the use of stem cells in therapy.

## Notes
### Contents
- Understandings
    - [U1]()
    - [U2]()
    - [U3]()
    - [U4]()
    - [U5]()
    - [U6]()
    - [U7]()
- Applications and Skills
    - [S1]()
    - [S2]()
    - [S3]()
    - [S4]()
    - [S5](#s5-calculating-magnification-and-general-optical-microscope-usage)

### U1: Cell Theory
* Basic Cell Theory Principles
    1. _All organisms are composed of one or more cells._
    2. _Cells are the smallest units of life._
    3. _All cells come from pre-existing cells._

### U2: Functions of Life
* Remember by _Mr. H. Gren_.
* __Metabolism__
* __Reproduction__
* __Homeostasis__
* __Growth__
* __Response__
* __Excretion__
* __Nutrition__

### S5: Calculating Magnification and General Optical Microscope Usage
* Microscope Magnifications: Optical lens (10x) multiplied by magnification lens.
    * __Scanning__ -- 40x
    * __Low__ -- 100x
    * __High__ -- 400x
* Units
    * Cells are generally measured in micrometers ($\mu m$).
        * Most eukaryotic cells are ~$100\mu m$ across.
* Drawings
    * All drawings _need_ a __title and magnification__.
* __Calculating Magnification and Actual Size__
    * Magnification = Image Size (disproportionate) / Actual Size (of the
      specimen)
        * $M = \frac{I}{A}$
    * Actual Size = Image Size / Magnification
        * $A = \frac{I}{M}$
* __Units__
    * 1nm = $1000\mu m$
    * 1cm = 1mm
