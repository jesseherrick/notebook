---
title: "Chapter 1.3: Membrane Structure"
date: 2016-01-15
published: true
subject: ibbio
---


## Curriculum
### Understandings
1. Phospholipids form bilayers in water due to the amphipathic
properties of phospholipid molecules.
2. Membrane proteins are diverse in terms of structure, position in
the membrane, and function.
3. Cholesterol is a component of animal cell membranes.

### Applications and Skills
1. Application: Cholesterol in mammalian membranes reduces membrane
   fluidity and permeability to some solutes.
2. Skill: Drawing of the fluid mosaic model.
3. Skill: Analysis of evidence from electron microscopy that led to the
   proposal of the Davson-Danielli model.
4. Skill: Analysis of the falsification of the Davson-Danielli model
   that led to the Singer-Nicolson model.

## Notes
### U1: The Phospholipid Bilayer
* The current model for the cell membrane is called the
  __*fluid mosaic model*__.
  * _All_ membranes have this structure.
* __Phospholipid__
  * The "backbone" of the membrane
  * Composed of __glycerol__ (3 carbon) with __fatty acids attached to 2
    of the glycerol carbons__ and an __organic alcohol attached to the
    other carbon__.
  * The __fatty acid__ side is __nonpolar__ and thus _hydrophobic_.
  * The __alcohol__ side is __polar__ and thus _hydrophilic_.
  * Because of these properties, __phospholipids are amphipathic__.
    - _a molecule that possesses both hydrophobic and hydrophilic elements_
* The regions of phospholipids align in a bilayer when water is present.
  * Because there is no attraction between the fatty acids on the
    inside of the bilayer, the bilayer is fluid.

### U2: Membrane Proteins
* Cause the diversity in membrane function.
* Proteins of various types are embedded in the fluid of the
phospholipid bilayer.
* __Integral Proteins__
  - __amphipathic__ -- both hydrophobic and hydrophilic regions within
    the protein
  - embedded throughout the protein (__*polytopic*__)
* __Peripheral proteins__
  - __hydrophilic__
  - bound to the surface of the membrane (__*monotopic*__)
  - often anchore to an integral protein
* Membrane functions (remember: __A PEACH__)
  - __Active transport via Pumps__ --- proteins ferry substances from
    one side of the membrane to the other by changing shape (requires ATP)
  - __Passive transport via Channels__ --- channels span the membrane
    and provide passage for substances; material moves from an area of
    _high concentration to an area of lower concentration_ (__*i.e.*__ diffusion)
  - __Enzymatic action__ --- enzymes may be on the interior or
    exterior of the cell and are often grouped to create a metabolic pathway
  - __Adhesion between cells__ --- proteins that hook together in
    various ways provide permanant or temporary connections; aka
    __*junctions*__ (can be tight junctions or gap junctions)
  - __Communication between cells__ --- _glycoproteins_ provide a
    chemical signature to identify themselves
  - __Hormone binding__ --- specific shapes and sizes are exposed tot
    he exterior that fit the shape of specific hormones
    - when the two attach, the shape of the protein changes which
      results in a message being relayed inside of the cell

### U3: Cholesterol in the Membrane (Animals)
* __More distance between the phosholipids = better fluidity.__
* higher temperature = more motion = more fluid
* **cholesterol** --- increases distance between phospholipids
  * at lower temperatures, this increases fluidity
  * at higher temperatures, this decreases fluidity
  * **this keeps cell membrane at a _stable fluidity_**

### UX: Saturated and Unsaturated Fats in the Membrane (Plants)
* __saturated fats__
  * the chain of fatty acids stacks on top of itself
  * thus, **decreasing fluidity**
* __unsaturated fats__
  * the double bond increases distance between the phospholipids
  * thus **increasing fluidity**

<iframe src="https://www.youtube.com/embed/BWQCAsM-CF4" frameborder="0" width="100%" height="auto" allowfullscreen></iframe>

### S3: Falsification of the Davson-Danielli model
* __The Davson-Danielli model of the phospholipid bilayer was wrong
  based on the following:__
  1. Not all proteins are identical or symetrical.
  2. Membranes with different functions also have different structures.
  3. A protein layer would interfere with cell function as most
  biological reactions are aqueous.
* The model also based its findings solely on electron microscope
  images, versus lab reactions with living cells.
