---
title: "Chapter 1.2: Cell Ultrastructure"
date: 2015-11-07
author: Jesse Herrick
subject: ibbio
---

## Curriculum
### Understandings
1. Prokaryotes have a simple cell structure without compartmentalization.
2. Eukaryotes have a compartmentalized cell structure.
3. Electron microscopes have a much higher resolution than light microscopes.

### Applications & Skills
1. Application: Structure and function of organelles within exocrine
gland cells of the pancreas and within palisade mesophyll cells of the
leaf.
2. Application: Prokaryotes divide by binary fission.
3. Skill: Drawing of the ultrastructure of prokaryotic cells based on
electron micrographs.
4. Skill: Drawing of the ultrastructure of eukaryotic cells based on
electron micrographs.
5. Skill: Interpretation of electron micrographs to identify
organelles and deduce the function of specialized cells.

### Guidance
1. Drawings of prokaryotic cells should show the cell wall, pili and
flagella, and plasma membrane enclosing cytoplasm that contains 70S
ribosomes and a nucleoid with naked DNA.
2. Drawings of eukaryotic cells should show a plasma membrane enclosing
cytoplasm that contains 80S ribosomes and a nucleus, mitochondria and
other membrane-bound organelles are present in the cytoplasm. Some
eukaryotic cells have a cell wall.

## Notes
* _What is **ulrastructure**?_ --- Structures too small to be studied/seen
  with just a microscope.

### U1: Prokaryotic Cells
- __cell wall__ --- protects and maintains cel shape
- __plasma membrane__ --- just inside of the cell wall; controls
movement into and out of the cell
- __cytoplasm__ --- aqueous solution inside of the cell
- __DNA/chromesome__ --- free-floating and not compartmentalized like
a eukaryotic cell
- __pili__ --- hair-like growths on the outside of the cell wall; used
for attachment
- __flagella__ --- allow cell to move (tail-like)

### U2: Eukaryotic Cells
### U3: Electron Microscopes and Resolution
