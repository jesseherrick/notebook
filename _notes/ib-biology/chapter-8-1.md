---
title: "Chapter 8.1: Metabolism"
date: 2015-10-27
subject: ibbio
---

## Curriculum
> [Psst!](https://ibpublishing.ibo.org/server2/rest/app/tsm.xql?doc=d_4_biolo_gui_1402_1_e&part=4&chapter=2)

### Understandings

1. Metabolic pathways consist of chains and cycles of enzyme-catalysed
   reactions.
2. Enzymes lower the activation energy of the chemical reactions that they
   catalyse.
3. Enzyme inhibitors can be competitive or non-competitive.
4. Metabolic pathways can be controlled by end-product inhibition.

### Application and Skills
1. Application: End-product inhibition of the pathway that converts threonine to
   isoleucine.
2. Application: Use of databases to identify potential new anti-malarial drugs.
3. Skill: Calculating and plotting rates of reaction from raw experimental
   results.
4. Skill: Distinguishing different types of inhibition from graphs at specified
   substrate concentration.

### Guidance
* Enzyme inhibition should be studied using one specific example for competitive
and non-competitive inhibition.

## Notes

### U1: Metabolic Pathway
* chemical changes happen in a series of small steps which form the **_metabolic
  pathway_**
  * several enzymes work together (indirectly) to complete the pathway
  * __*e.g.*__ _molecule_ &rarr; reaction 1 &rarr; reaction 2 &rarr; ... &rarr; product

### U2: Enzymes
[![C'mon, son!](//media.giphy.com/media/89No6UHzuUam4/giphy.gif)](/note/ib-biology/chapter-2-5/)

### U3: Enzyme Inhibition
* __*inhibitors*__ --- molecules that selectively inhibit enzymes
* Types:
  1. __*competitive inhibition*__
  2. __*non-competitive inhibition*__
* ![Enzyme Inhibition Figure](https://figures.boundless.com/18822/raw/figure-06-05-04.jpeg)

#### Competitive Inhibition
* inhibitor is structurally similar to substrate
  * so the inhibitor fits its enzyme's active site
* lowers reaction rate
* __If inhibitor concentration is low, increasing the substrate concentration
  *will reduce the inhibition*.__

#### Non-Competitive Inhibition
* _unlike_ the substrate
* binds to an enzyme's __*allosteric site*__
* changes the shape of an enzyme's active site (permanently disabling the
  enzyme)
* __If the inhibitor is low, increasing substrate concentration has *no
  effect*.__

### U4: End Product Inhibition
* the end product of a metabolic pathway reaction _is_ an inhibitor itself
  * it causes the pathway to stop producing product
  * ![End-product Inhibition](https://figures.boundless.com/18823/raw/figure-06-05-05.jpeg)

### A1: Threonine to Isoleucine
* pathway converting amino acid threonine to lucine
* start with threonine and deaminase
  * 5 enzymes later...
  * isoleucine is produced
  * as isoleucine concentration builds, it binds to the allosteric site of the
    1st enzyme (threonine deaminase) in the pathway
