---
date: 2015-10-07
title: "Chapter 2.4: Proteins"
subject: ibbio
---

## IB Curriculum
### Understandings
1. [Amino acids are linked together by condensation to form polypeptides.](#u1-condensation-reactions-make-polypeptides)
2. [There are 20 different amino acids in polypeptides synthesized on ribosomes.](#u2-amino-acid-info)
3. [Amino acids can be linked together in any sequence giving a huge range of
  possible polypeptides.](#u3-amino-acid-diversity-in-relation-to-polypeptides)
4. [The amino acid sequence of polypeptides is coded for by genes.](#u4-the-central-dogma-of-genetics)
5. [A protein may consist of a single polypeptide or more than one polypeptide
  linked together.](#u5-what-makes-up-a-protein)
6. [The amino acid sequence determines the three-dimensional conformation of a
  protein.](#u6-proteins-in-3d)
7. [Living organisms synthesize many different proteins with a wide range of
  functions.](#u7-the-non-exclusive-list-of-protein-functions)
8. [Every individual has a unique proteome.](#u8-individuals-have-unique-proteome)

### Applications and Skills
1. Application: Rubisco, insulin, immunoglobulins, rhodopsin, collagen and spider
  silk as examples of the range of protein functions.
2. Application: Denaturation of proteins by heat or by deviation of pH from the
  optimum.
3. Skill: Drawing molecular diagrams to show the formation of a peptide bond.

### Guidance
1. The detailed structure of the six proteins selected to illustrate the
  functions of proteins is not needed.
2. Egg white or albumin solutions can be used in denaturation experiments.
3. Students should know that most organisms use the same 20 amino acids in the
  same genetic code although there are some exceptions. Specific examples could
  be used for illustration.

### Utilization
1. Proteomics and the production of proteins by cells cultured in fermenters
  offer many opportunities for the food, pharmaceutical and other industries.

### Aims
1. Aim 7: ICT can be used for molecular visualization of the structure of
  proteins.
2. Aim 8: Obtaining samples of human blood for immunological, pharmaceutical and
  anthropological studies is an international endeavour with many ethical
  issues.

## Notes
### U1: Condensation Reactions Make Polypeptides
* **_polypeptide_** --- 2 or more amino acids bonded through _condensation
  reactions_
* **_peptide bond_** --- the bond between two amino acids in a polypeptide

### U2: Amino Acid Info
* There are a total of 20 amino acids in polypeptides synthesized on ribosomes.
* There are 22 proteinogenic amino acids encoded in all organisms.
* Amino acids typically end with the __*-ine*__ suffix.

### U3: Amino Acid Diversity in relation to Polypeptides
* Amino acid chains can be of any sequence
* Amino acid chains can be of any length

### U4: The Central Dogma of Genetics
* **DNA** &rarr; **RNA** &rarr; **polypeptide**
  * **_transcription_** &rarr; **_translation_**
  * **DNA** is stored in the nucleus
  * **RNA** writes it down and makes (for ribosomes)
  * **Amino acids** into **polypeptides**

### U5: What makes up a protein?

### U6: Proteins... in 3D!?!
* Properties of amino acids determines how a polypeptide folds up into a
  protein.
* **Structures**
  1. **Primary Structure**
      * _the amino acid sequence_
      * amino acids abbreviated with letters
  2. **Secondary Structure**
      * _stabilized by_ repeating H-bonds
      * $\alpha$ helix and $\beta$ pleated sheets
  3. **Tertiary Structure**
      * _stabilized by_
          * H-bonds
          * ionic bonds
          * disulfide bridges
          * Van-der-Wals bands (_intramolecular forces_)
  4. **Quaternary Structure**
      * _just linked tertiary structures_
      * stabilized in the same way as tertiary

### U7: The Non-exclusive List of Protein Functions
1. structure (__*e.g.*__ muscles)
2. movement (__*e.g.*__ muscle contraction)
3. transport (__*e.g.*__ lipoprotein complexes)
4. defense (__*e.g.*__ immunoglobulin)
5. signalling (__*e.g.*__ hormones)
6. catalysis (__*e.g.*__ enzymes)
7. cell membrane transport
8. tensile strengthening (__*e.g.*__ muscles stretching)
9. clotting factors (__*e.g.*__ Von Willebrand clotting factor: vWf)
10. receptors (__i.e.__ guide and receive signals)


### U8: Individuals have Unique Proteome
* __*proteome*__ --- all of the _proteins_ produced by a cell, tissue, or
  organism
* __*genome*__ --- all of the _genes_ of a cell, tissue, or organism
* Environmental factors effect the way proteins in organisms/cells work. Factors
  like: 
    * nutrition
    * temperature
    * activity levels

### A2: The Denaturation of Proteins
* **_denaturation_** --- changing a protein's native shape
* H-bonds, disulfide bridges, ionic bonds, etc. break
* __Due to:__
    * extremes of pH (cause R-group charges to change, which breaks ionic bonds
    within the protein and causes new ones to form)
    * high temperature (molecular vibrations break bonds and/or interactions of
    side chains)
    * organic solvent
* __*Denaturation is usually permanent.*__
  
