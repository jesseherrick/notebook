---
title: "Chapter 2.5: Enzymes"
date: 2015-10-14
subject: ibbio
---

## Curriculum
__Essential Idea:__ _Enzymes control the metabolism of the cell._

### Understandings
1. Enzymes have an active site to which specific substrates bind.
2. Enzyme catalysis involves molecular motion and the collision of substrates
   with the active site.
3. Temperature, pH and substrate concentration affect the rate of activity of
   enzymes.
4. Enzymes can be denatured.
5. Immobilized enzymes are widely used in industry.

### Applications and Skills
1. Application: Methods of production of lactose-free milk and its advantages.
2. Skill: Design of experiments to test the effect of temperature, pH and
   substrate concentration on the activity of enzymes.
3. Skill: Experimental investigation of a factor affecting enzyme activity.
   (Practical 3)

### Guidance
1. Lactase can be immobilized in alginate beads and experiments can then be
   carried out in which the lactose in milk is hydrolysed.
2. Students should be able to sketch graphs to show the expected effects of
   temperature, pH and substrate concentration on the activity of enzymes. They
   should be able to explain the patterns or trends apparent in these graphs.

## Notes
### U1: Enzymes have active sites for substrates.
- __*enzyme*__ --- biological catalysts
- __*active site*__ --- place where substrates bind
- __*substrate*__ --- substance that an enzyme binds with and acts on
- __*Enzymes lower activation energy needed by cells.*__
    - And thus, _increase reaction rate_.
- Enzymes usually end in __*--ase*__ (__*e.g.*__ _carbohydrase_)

#### Enzyme Catalysis Process
1. attract (substrates are chemically attracted to the active-site)
2. attach
  - when substrates attach to the active site, the joint compound is called an
  __*enzyme-substrate complex*__
3. react (__*i.e.*__ _catalysis_)
4. release (__Note:__ _The enzyme is unchanged._)

#### Enzyme Theories
- __Older Theory:__ _Lock and Key_ Model
    - _one substrate_ per enzyme (and vice-versa)
        - __*Why is this important?*__ Because we don't want too many reactions
          going on at the same time _and_ we don't want enzymes reacting on
          things that don't need to be reacted.
        - they chemically and structurally fit

- __Newer Theory:__ _Induced Fit (Glove)_ Model
    - The enzyme changes shape _slightly_ to fit better to substrates.
    - This _"hug"_ stresses the substrate which further lowers activation
      energy.

### U2: Enzyme Kinetics
- __*enzyme kinetics*__ --- studying rate at which enzymes work
    - substrates and enzymes _must_ collide in the right orientation in order to
      react
        - most reactions occur in water
    - __*collisions happen due to random motion*__
    - But the real question... _Are enzymes micro-sarlacs?_ (hint: probably not
      because they don't move on their own)
    - successful collisions are those where the active site and substrate are
      aligned properly

### U3: Variables Affecting Enzyme Activity
- __temperature__
    - ![Enzyme Temperature Graph](http://plantphys.info/plant_physiology/images/enztemprate.gif)
    - heat speeds up the molecules, thus increasing collision rate
- __pH__
    - **every enzyme has its own optimum pH**
    - they shouldn't stray too far from this
- __substrate concentration__
    - more substrates means more things to collide with
    - until a ceiling is hit where every enzyme is in use
    - *there is a hyperbolic relationship between substrate concentration and
      rate of reaction
    - ![Substrate Concentration-Rate Relationship](https://www.ucl.ac.uk/~ucbcdab/enzass/images/subs1.png)

### U4: The Integrity of Enzymes
- enzymes function best in a small range of conditions (see U3 above)
- **enzyme _function_ depends on its _structure_**
    - so if an enzyme is _denatured_ (loss of structural integrity), then its
      active site will no longer be able to bind with the proper substrates
    - **broken active site bonds = broken enzyme**

### U5: Immobilized Enzymes
- *restricting enzyme mobility in a fixed space*
    - *via:* entrapment in a __matrix__ or __droplets__
- **Industry Usage**
    1. detergent (__*e.g.*__ proteases and lipases to break down proteins and
       lipids)
    2. biofuels (__*e.g.*__ enzymes breaking down starch into sugar)
    3. Medicine/Bio Technology (__*e.g.*__ cutting DNA to splice genes _or_
       contact lens solution)
    4. food production
        - pectin (__*e.g.*__ fruit juice into jelly)
        - rennin (__*e.g.*__ milk into cheese)
        - isomerase (__*e.g.*__ glucose into fructose)
    5. textiles (__*.e.g*__ processing fibers into cloth)
- **Advantages**
    - enzymes are easily separated
    - _you_ control the reaction time
    - *enzymes can be recycled*
    - higher concentration of enzyme speeds up reaction rate
    - increase the stability of enzymes to changes in temperature and pH

